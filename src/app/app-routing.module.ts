import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListCasasComponent } from './casas/list-casas/list-casas.component';
import { ListaEstudiantesComponent } from './lista-estudiantes/lista-estudiantes.component';

const routes: Routes = [
  {path: '', redirectTo:'/casas', pathMatch: 'full'},
  {path: 'casas', component: ListCasasComponent},
  {path: 'estudiantes/:casa', component: ListaEstudiantesComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
