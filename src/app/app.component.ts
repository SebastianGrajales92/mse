import { Component, ViewChild } from '@angular/core';
import { CasaComponent } from './casas/casa/casa.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {

  title = 'harry-potter';
}
