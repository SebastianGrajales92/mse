import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';
import { Student } from 'src/models/student-model';
import { environment } from 'src/environments/environment';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CasaServiceService {

  constructor(private http: HttpClient) { }

  getStudents(nombreCasa:string):Observable<any>{
    return this.http.get(`${environment.enlaceApiCasas}${nombreCasa}`).pipe(
      map(response=> response as Student[])
    )
  }
}
