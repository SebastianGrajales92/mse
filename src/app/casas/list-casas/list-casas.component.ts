import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-casas',
  templateUrl: './list-casas.component.html',
  styleUrls: ['./list-casas.component.sass']
})
export class ListCasasComponent implements OnInit {

  constructor() { }

  lstCasas: Array<string> = ["Gryffindor","Hufflepuff","Ravenclaw","Slytherin"];

  ngOnInit(): void {
  }

}
