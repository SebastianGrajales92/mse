import { Component, Input, OnInit} from '@angular/core';
import { Student } from 'src/models/student-model';
import { CasaServiceService } from './casa-service.service';

@Component({
  selector: 'app-casa',
  templateUrl: './casa.component.html',
  styleUrls: ['./casa.component.sass']
})
export class CasaComponent implements OnInit {

  @Input() nombreCasa;
  students: Array<Student>;
  urlImageCasa:string;
  constructor(private casaService:CasaServiceService) { }

  ngOnInit(): void {
    this.urlImageCasa=`../../assets/img/${this.nombreCasa}.jpg`;
    this.getStudents();
  }

  private getStudents(){
    this.casaService.getStudents(this.nombreCasa).subscribe(
      response=>{
        this.students = response;
      }
    );
  }

}
