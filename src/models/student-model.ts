export class Student {
   name:string;
   lastName:string;
   species:string;
   gender:string;
   house:string;
   dateOfBirth:string;
   yearOfBirth:string;
   ancestry:string;
   eyeColour:string;
   hairColour:string;
   patronus:string;
   hogwartsStudent: boolean;
   hogwartsStaff: boolean   ;
   actor:string;
   alive: boolean;
   image:string;
   bloodStatus:string;

   constructor(){
       this.name = "";
       this.lastName= "";
       this.species = "";
       this.gender = "";
       this.house = "";
       this.dateOfBirth = "";
       this.yearOfBirth = "";
       this.ancestry = "";
       this.eyeColour = "";
       this.hairColour = "";
       this.patronus = "";
       this.hogwartsStaff = false;
       this.hogwartsStudent = false;
       this.actor = "";
       this.alive = false;
       this.image = "";
       this.bloodStatus = "";
   }
}