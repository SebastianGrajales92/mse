import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Student } from 'src/models/student-model';
import { CasaServiceService } from '../casas/casa/casa-service.service';
import { ModalService } from '../detalle-estudiante/modal.service';

@Component({
  selector: 'app-lista-estudiantes',
  templateUrl: './lista-estudiantes.component.html',
  styleUrls: ['./lista-estudiantes.component.sass']
})
export class ListaEstudiantesComponent implements OnInit {

  nombreCasa: string;
  lstStudents: Array<Student>;
  lstStudentsAux: Array<Student>;
  student: Student;
  ordenarName: false;
  nombreBuscar: string;
  constructor(private activatedRoute: ActivatedRoute, private casaService: CasaServiceService, public modalService: ModalService) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(params => {
      this.nombreCasa = params.get('casa')
    });
    this.getStudents();
  }

  private getStudents() {
    this.casaService.getStudents(this.nombreCasa).subscribe(response => {
      this.lstStudentsAux = response;
      this.lstStudentsAux.map((studen: Student) => {
        let nameLastName = studen.name.split(" ");
        studen.name = nameLastName[0];
        studen.lastName = nameLastName[1];
        if (studen.ancestry != '')
          studen.bloodStatus = `../../assets/img/${studen.ancestry}.jpg`;

        else
          studen.bloodStatus = `../../assets/img/dementor.jpg`;
      });
      this.lstStudents = this.lstStudentsAux;
    });
  }

  abrirModal(student: Student) {
    this.student = student;
    this.modalService.abrirModal();
  }

  ordenarNombre(): void {
    this.lstStudents = this.lstStudentsAux.sort((a, b) => {
      if (a.name > b.name) {
        return 1;
      }
      if (a.name < b.name) {
        return -1;
      }
      return 0;
    }).slice();
  }

  ordenarApellido(): void {
    this.lstStudents = this.lstStudentsAux.sort((a, b) => {
      if (a.lastName > b.lastName) {
        return 1;
      }
      if (a.lastName < b.lastName) {
        return -1;
      }
      return 0;
    }).slice();
  }

  filtrar(): void {
    if (this.nombreBuscar == "") {
      this.lstStudents = this.lstStudentsAux;
    }
    else {
      this.lstStudentsAux.forEach(student => {
        this.lstStudents = this.lstStudentsAux.filter(studenAux => studenAux.name.toLowerCase().includes(this.nombreBuscar.toLowerCase()));
      });
    }
  }

  borrarFiltrar(){
    this.lstStudents = this.lstStudentsAux;
    this.nombreBuscar = '';
  }
}
