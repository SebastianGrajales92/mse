import { Component, Input, OnInit } from '@angular/core';
import { Student } from 'src/models/student-model';
import { ModalService } from './modal.service';

@Component({
  selector: 'app-detalle-estudiante',
  templateUrl: './detalle-estudiante.component.html',
  styleUrls: ['./detalle-estudiante.component.sass']
})
export class DetalleEstudianteComponent implements OnInit {

  @Input() student:Student;
  titulo:string = 'Detalle del estudiante';
  constructor(public modalService: ModalService) { }

  ngOnInit(): void {
  }

  cerrarModal(){
    this.modalService.cerrarModal();
  }

}
